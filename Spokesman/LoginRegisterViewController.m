//
//  LoginRegisterViewController.m
//  Spokesman
//
//  Created by chaitanya venneti on 23/01/16.
//  Copyright © 2016 troomobile. All rights reserved.
//

#import "LoginRegisterViewController.h"
#import "AppConstant.h"
#import "MainCanvasViewController.h"

@interface LoginRegisterViewController ()

@end

@implementation LoginRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    [_loginRegisterView setWantsLayer:YES];
    [_loginRegisterView.layer setBackgroundColor:[[NSColor colorWithCalibratedRed:0/255 green:17/255 blue:24/255 alpha:1] CGColor]];
    
    //_txtSignupUsername.delegate = self;
    
    [self setButtonTitle:_btnLogin toString:@"LOGIN" withColor:[NSColor whiteColor] withSize:18];
    [self setButtonTitle:_btnShowLogin toString:@"LOGIN" withColor:[NSColor whiteColor] withSize:18];
    [self setButtonTitle:_btnSignup toString:@"SIGN UP" withColor:[NSColor whiteColor] withSize:18];
    [self setButtonTitle:_btnShowSignUp toString:@"SIGN UP" withColor:[NSColor whiteColor] withSize:18];
    
    [self setPlaceholderTitle:_txtSignupUsername toString:@"USERNAME" withColor:[NSColor whiteColor] withSize:14];
    [self setPlaceholderTitle:_txtSignupEmail toString:@"EMAIL" withColor:[NSColor whiteColor] withSize:14];
    [self setPlaceholderTitle:_txtSignupPassword toString:@"PASSWORD" withColor:[NSColor whiteColor] withSize:14];
    [self setPlaceholderTitle:_txtSignupConfirm toString:@"CONFIRM PASSWORD" withColor:[NSColor whiteColor] withSize:14];
    
    [self setPlaceholderTitle:_txtLoginUsername toString:@"USERNAME" withColor:[NSColor whiteColor] withSize:14];
    [self setPlaceholderTitle:_txtLoginPassword toString:@"PASSWORD" withColor:[NSColor whiteColor] withSize:14];
    
    
}

- (NSRect)screenRectEquivelentOfView:(NSView*)view
{
    //NSWindow* viewWindow = [view window];
    
    NSRect frame = [view frame];
    frame = [[view superview] convertRect:frame toView:nil];
    //frame.origin = [viewWindow convertBaseToScreen:frame.origin];
    frame.origin = CGPointZero;
    return (frame);
}

-(void)viewWillAppear{
    [super viewWillAppear];
    
    if(_showLogin)
    {
        _loginbox.hidden = false;
        _signupbox.hidden = true;
    }
    else
    {
        _loginbox.hidden = true;
        _signupbox.hidden = false;
    }
}

- (IBAction)btnSignupClick:(id)sender {
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ([emailTest evaluateWithObject:_txtSignupEmail.stringValue] == NO) {
        
        NSAlert *alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:@"OK"];
        [alert setMessageText:@"Invalid Email!"];
        [alert setInformativeText:@"Please enter a valid email address."];
        [alert setAlertStyle:NSWarningAlertStyle];
        
        if ([alert runModal] == NSAlertFirstButtonReturn) {
            // OK clicked, delete the record
        }
        
        return;
    }
    
    if (_txtSignupUsername.stringValue.length == 0) {
        
        NSAlert *alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:@"OK"];
        [alert setMessageText:@"Invalid Username!"];
        [alert setInformativeText:@"Username cannot be empty."];
        [alert setAlertStyle:NSWarningAlertStyle];
        
        if ([alert runModal] == NSAlertFirstButtonReturn) {
            // OK clicked, delete the record
        }
        
        return;
    }
    
    if (_txtSignupPassword.stringValue.length == 0) {
        
        NSAlert *alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:@"OK"];
        [alert setMessageText:@"Invalid Password!"];
        [alert setInformativeText:@"Password cannot be empty."];
        [alert setAlertStyle:NSWarningAlertStyle];
        
        if ([alert runModal] == NSAlertFirstButtonReturn) {
            // OK clicked, delete the record
        }
        
        return;
    }
    
    if (![_txtSignupPassword.stringValue isEqualToString:_txtSignupConfirm.stringValue]) {
        
        NSAlert *alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:@"OK"];
        [alert setMessageText:@"Invalid Passwords!"];
        [alert setInformativeText:@"Passwords does not match."];
        [alert setAlertStyle:NSWarningAlertStyle];
        
        if ([alert runModal] == NSAlertFirstButtonReturn) {
            // OK clicked, delete the record
        }
        
        return;
    }
    
    [self showMainCanvas:_txtSignupUsername.stringValue];
}

- (IBAction)btnShowLoginClick:(id)sender {
    _loginbox.hidden = false;
    _signupbox.hidden = true;
}
- (IBAction)btnLoginClick:(id)sender {
    
    if (_txtLoginUsername.stringValue.length == 0) {
        
        NSAlert *alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:@"OK"];
        [alert setMessageText:@"Invalid Username!"];
        [alert setInformativeText:@"Username cannot be empty."];
        [alert setAlertStyle:NSWarningAlertStyle];
        
        if ([alert runModal] == NSAlertFirstButtonReturn) {
            // OK clicked, delete the record
        }
        
        return;
    }
    
    if (_txtLoginPassword.stringValue.length == 0) {
        
        NSAlert *alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:@"OK"];
        [alert setMessageText:@"Invalid Password!"];
        [alert setInformativeText:@"Password cannot be empty."];
        [alert setAlertStyle:NSWarningAlertStyle];
        
        if ([alert runModal] == NSAlertFirstButtonReturn) {
            // OK clicked, delete the record
        }
        
        return;
    }
    
    [self showMainCanvas:_txtLoginUsername.stringValue];
}

-(void)showMainCanvas:(NSString*)username{
    MainCanvasViewController *canvasViewController = [self.storyboard instantiateControllerWithIdentifier:@"MainCanvasViewController"];
    
    if(username.length == 0)
        username = @"Username";
    
    canvasViewController.username = username;
    NSRect _mainframe = [[NSScreen mainScreen] visibleFrame];
    [[canvasViewController view] setFrame:_mainframe];
    self.view.window.contentViewController = canvasViewController;
}

- (IBAction)btnShowSignUpClick:(id)sender {
    _loginbox.hidden = true;
    _signupbox.hidden = false;
}

- (IBAction)btnForgotPasswordClick:(id)sender {
}

-(void)setButtonTitle:(NSButton*)button toString:(NSString*)title withColor:(NSColor*)color withSize:(int)size{
    NSFont *txtFont = [NSFont systemFontOfSize:size];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    NSDictionary *txtDict = [NSDictionary dictionaryWithObjectsAndKeys:
                             txtFont, NSFontAttributeName, color, NSForegroundColorAttributeName, nil];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:title attributes:txtDict];
    [attrStr addAttributes:[NSDictionary dictionaryWithObject:paragraphStyle forKey:NSParagraphStyleAttributeName] range:NSMakeRange(0,[attrStr length])];
    [button setAttributedTitle:attrStr];
}

-(void)setPlaceholderTitle:(NSTextField*)txtField toString:(NSString*)title withColor:(NSColor*)color withSize:(int)size{
    NSFont *txtFont = [NSFont systemFontOfSize:size];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
    [paragraphStyle setAlignment:NSTextAlignmentLeft];
    
    NSDictionary *txtDict = [NSDictionary dictionaryWithObjectsAndKeys:
                             txtFont, NSFontAttributeName, color, NSForegroundColorAttributeName, nil];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:title attributes:txtDict];
    [attrStr addAttributes:[NSDictionary dictionaryWithObject:paragraphStyle forKey:NSParagraphStyleAttributeName] range:NSMakeRange(0,[attrStr length])];
    
    //[attrStr addAttribute: NSBaselineOffsetAttributeName value: [NSNumber numberWithFloat: -10.0] range: NSMakeRange(0, [attrStr length])];
    
    [txtField setPlaceholderAttributedString:attrStr];
}



@end
