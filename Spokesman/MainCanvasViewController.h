//
//  MainCanvasViewController.h
//  Spokesman
//
//  Created by chaitanya venneti on 24/01/16.
//  Copyright © 2016 troomobile. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MainCanvasViewController : NSViewController

@property NSString* username;

@property (strong) IBOutlet NSView *mainView;

@property (weak) IBOutlet NSButton *btnBrowse;
@property (weak) IBOutlet NSButton *btnConform;
@property (weak) IBOutlet NSButton *btnEmbed;
@property (weak) IBOutlet NSButton *btnExport;
@property (weak) IBOutlet NSImageView *imgBrowseIndicator;
@property (weak) IBOutlet NSImageView *imgConformIndicator;
@property (weak) IBOutlet NSImageView *imgEmbedIndicator;
@property (weak) IBOutlet NSImageView *imgExportIndicator;

@property (weak) IBOutlet NSBox *boxBrowseMenuItems;

- (IBAction)btnBrowseClick:(id)sender;
- (IBAction)btnConformClick:(id)sender;
- (IBAction)btnEmbedClick:(id)sender;
- (IBAction)btnExportClick:(id)sender;

@property (weak) IBOutlet NSTextField *lblMessage;

@property (weak) IBOutlet NSTextField *lblUsername;

@end
