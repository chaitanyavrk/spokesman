//
//  MainCanvasViewController.m
//  Spokesman
//
//  Created by chaitanya venneti on 24/01/16.
//  Copyright © 2016 troomobile. All rights reserved.
//

#import "MainCanvasViewController.h"

@interface MainCanvasViewController ()

@end

@implementation MainCanvasViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    [_mainView setWantsLayer:YES];
    [_mainView.layer setBackgroundColor:[[NSColor colorWithCalibratedRed:0/255 green:17/255 blue:24/255 alpha:1] CGColor]];
    [self toggleButtonState:@"browse"];
    _lblUsername.stringValue = _username;
    
}

- (IBAction)btnBrowseClick:(id)sender {
    //if(_btnBrowse.state == 0)
        [self toggleButtonState:@"browse"];
}
- (IBAction)btnConformClick:(id)sender {
    //if(_btnConform.state == 0)
        [self toggleButtonState:@"conform"];
}
- (IBAction)btnEmbedClick:(id)sender {
    //if(_btnEmbed.state == 0)
        [self toggleButtonState:@"embed"];
}
- (IBAction)btnExportClick:(id)sender {
    //if(_btnExport.state == 0)
        [self toggleButtonState:@"export"];
}

-(void)toggleButtonState:(NSString*)buttonName{
    
    _btnBrowse.state = 0;
    NSMutableAttributedString *_string = [self getAttributedString:@"    Browse" color:[NSColor whiteColor]];
    [_btnBrowse setAttributedTitle: _string];
    [_btnBrowse setAttributedAlternateTitle:_string];
    _imgBrowseIndicator.hidden = true;
    _boxBrowseMenuItems.hidden = true;
    
    _string = [self getAttributedString:@"    Conform" color:[NSColor whiteColor]];
    _btnConform.state = 0;
    [_btnConform setAttributedTitle:_string];
    [_btnConform setAttributedAlternateTitle:_string];
    _imgConformIndicator.hidden = true;
    
    
    _btnEmbed.state = 0;
    _string = [self getAttributedString:@"    Embed" color:[NSColor whiteColor]];
    [_btnEmbed setAttributedTitle:_string];
    [_btnEmbed setAttributedAlternateTitle:_string];
    _imgEmbedIndicator.hidden = true;
    
    _btnExport.state = 0;
    _string = [self getAttributedString:@"    Export" color:[NSColor whiteColor]];
    [_btnExport setAttributedTitle:_string];
    [_btnExport setAttributedAlternateTitle:_string];
    _imgExportIndicator.hidden = true;
    
    _lblMessage.hidden = false;
    
    if([buttonName isEqualToString:@"browse"])
    {
        _btnBrowse.state = 1;
        _string = [self getAttributedString:@"    Browse" color:[NSColor orangeColor]];
        [_btnBrowse setAttributedTitle:_string];
        [_btnBrowse setAttributedAlternateTitle:_string];
        _imgBrowseIndicator.hidden = false;
        _boxBrowseMenuItems.hidden = false;
        
        _lblMessage.hidden = true;
    }
    if([buttonName isEqualToString:@"conform"]){
        _btnConform.state = 1;
        _string = [self getAttributedString:@"    Conform" color:[NSColor orangeColor]];
        [_btnConform setAttributedTitle:_string];
        [_btnConform setAttributedAlternateTitle:_string];
        _imgConformIndicator.hidden = false;
     
    }
    if([buttonName isEqualToString:@"embed"]){
        _btnEmbed.state = 1;
        _string = [self getAttributedString:@"    Embed" color:[NSColor orangeColor]];
        [_btnEmbed setAttributedTitle:_string];
        [_btnEmbed setAttributedAlternateTitle:_string];
        _imgEmbedIndicator.hidden = false;

    }
    if([buttonName isEqualToString:@"export"]){
        _btnExport.state = 1;
        _string = [self getAttributedString:@"    Export" color:[NSColor orangeColor]];
        [_btnExport setAttributedTitle:_string];
        [_btnExport setAttributedAlternateTitle:_string];
        _imgExportIndicator.hidden = false;

    }
}

-(NSMutableAttributedString*)getAttributedString:(NSString*)title color:(NSColor*)withColor
{
    NSFont *txtFont = [NSFont systemFontOfSize:18];
    
    NSDictionary *txtDict = [NSDictionary dictionaryWithObjectsAndKeys: txtFont, NSFontAttributeName,
                         withColor, NSForegroundColorAttributeName, nil];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:title attributes:txtDict];
    return attrStr;
}
@end
