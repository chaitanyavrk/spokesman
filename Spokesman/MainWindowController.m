//
//  MainWindowController.m
//  Spokesman
//
//  Created by chaitanya venneti on 23/01/16.
//  Copyright © 2016 troomobile. All rights reserved.
//

#import "MainWindowController.h"
#import "ViewController.h"
@interface MainWindowController ()

@end

@implementation MainWindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    
    ViewController *viewController = [self.storyboard instantiateControllerWithIdentifier:@"LandingPageViewController"];
    NSRect _mainframe = [[NSScreen mainScreen] visibleFrame];
    [[viewController view] setFrame:_mainframe];
    self.window.contentViewController = viewController;
    [self.window zoom:NULL];
}

-(void)windowWillLoad{
    [super windowWillLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    NSRect _fr = [[NSScreen mainScreen] visibleFrame];
    
    [_mainWindow setFrame:_fr display:YES];
    [_mainWindow setContentSize:_fr.size];
}

@end
