//
//  ViewController.h
//  Spokesman
//
//  Created by chaitanya venneti on 23/01/16.
//  Copyright © 2016 troomobile. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController

@property (strong) IBOutlet NSView *landingPageView;
@property (weak) IBOutlet NSButton *btnLogin;
@property (weak) IBOutlet NSButton *btnSignup;

- (IBAction)btnLoginClick:(id)sender;
- (IBAction)btnSignupClick:(id)sender;
@end

