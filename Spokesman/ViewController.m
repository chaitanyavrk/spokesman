//
//  ViewController.m
//  Spokesman
//
//  Created by chaitanya venneti on 23/01/16.
//  Copyright © 2016 troomobile. All rights reserved.
//

#import "ViewController.h"
#import "LoginRegisterViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    [self setButtonTitle:_btnLogin toString:@"LOGIN" withColor:[NSColor whiteColor] withSize:18];
    [self setButtonTitle:_btnSignup toString:@"SIGN UP" withColor:[NSColor whiteColor] withSize:18];

}

-(void)setButtonTitle:(NSButton*)button toString:(NSString*)title withColor:(NSColor*)color withSize:(int)size{
    NSFont *txtFont = [NSFont systemFontOfSize:size];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    NSDictionary *txtDict = [NSDictionary dictionaryWithObjectsAndKeys:
                             txtFont, NSFontAttributeName, color, NSForegroundColorAttributeName, nil];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:title attributes:txtDict];
    [attrStr addAttributes:[NSDictionary dictionaryWithObject:paragraphStyle forKey:NSParagraphStyleAttributeName] range:NSMakeRange(0,[attrStr length])];
    [button setAttributedTitle:attrStr];
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
    
}

-(void)viewWillAppear{
    [super viewWillAppear];
    
    NSWindow* _window = [_landingPageView window];
    
    NSRect _mainframe = [[NSScreen mainScreen] visibleFrame];
    [_landingPageView setFrame:_mainframe];
    
    [_window setContentSize:_landingPageView.frame.size];
    NSRect viewScreenFrame = [self screenRectEquivelentOfView:_landingPageView];
    NSRect wndFrame = [_window frameRectForContentRect:viewScreenFrame];
    [_window setFrame:wndFrame display:YES animate:YES];
    [_window zoom:NULL];
    
    [_landingPageView setWantsLayer:YES];
    [_landingPageView.layer setBackgroundColor:[[NSColor blackColor] CGColor]];
    
}

- (NSRect)screenRectEquivelentOfView:(NSView*)view
{
    NSWindow* viewWindow = [_landingPageView window];
    NSRect frame = [view frame];
    frame = [[view superview] convertRect:frame toView:nil];
    //frame.origin = [viewWindow convertBaseToScreen:frame.origin];;
    frame = [viewWindow convertRectToScreen:frame];
    return (frame);
}

- (IBAction)btnLoginClick:(id)sender {
    [self showLoginSignup:true];
}

-(void)showLoginSignup:(BOOL)showLogin{
    LoginRegisterViewController *loginViewController = [self.storyboard instantiateControllerWithIdentifier:@"LoginRegisterViewController"];
    loginViewController.showLogin = showLogin;
    NSRect _mainframe = [[NSScreen mainScreen] visibleFrame];
    [[loginViewController view] setFrame:_mainframe];
    self.view.window.contentViewController = loginViewController;
}

- (IBAction)btnSignupClick:(id)sender {
    [self showLoginSignup:false];
}
@end
